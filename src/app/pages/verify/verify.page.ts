import { Component, OnInit } from '@angular/core';
import { MenuController, NavController, Platform, ToastController, LoadingController, IonSlides, DomController, ModalController } from '@ionic/angular';
import { Router } from  "@angular/router";
import { Storage } from '@ionic/storage';
import { map } from 'rxjs/operators';
import { EnvService } from 'src/app/services/env.service';


@Component({
  selector: 'app-verify',
  templateUrl: './verify.page.html',
  styleUrls: ['./verify.page.scss'],
})
export class VerifyPage implements OnInit {
  email: any;
  constructor(public navCtrl: NavController, public modelCtrl: ModalController, private router: Router, public loadingCtrl: LoadingController, 
    private storage: Storage, private service:EnvService, private toastCtrl:ToastController ) { 


  }

  ngOnInit() {
      this.storage.get('email').then((result) => { 
        this.email = result;
      });
      //console.log('Entro a verificar.......');
  }

  Pin: String ="";
  ShowPin: Boolean = false;
  
  eventCapture(event) {
    this.ShowPin = false;
    this.Pin=event;
    //console.log(this.Pin);
     let params = {
      email: this.email,
      codigo: this.Pin
    }

    this.storage.get('token').then((token) => {
     this.service.post_token('verificar', params, token)
     .pipe(map(data => {    
        this.storage.set('email_verified_at', 'true');
       //console.log(data); 
       this.showToast('¡El usuario se a verificado exitosamente!');
       this.router.navigate(['/dashboard']);     
     }))     
     .subscribe(
       (data) => {       
            this.router.navigate(['/dashboard']);     
         },
         (err) => {
           //console.log(err);
           let error = err['error'].msg;
           //console.error(error);
           this.showToast(error);
       });
     });

  }

  showPin() {
    this.ShowPin = !this.ShowPin;
    //console.log(this.Pin);
  }

  omitir(){
     this.router.navigate(['/dashboard']);
  }

   async showToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}
