import { Component, OnInit } from '@angular/core';
import { MenuController, NavController, Platform, ToastController, LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Router } from  "@angular/router";
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { EnvService } from 'src/app/services/env.service';
import { AlertService } from 'src/app/services/alert.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit {
  address: { id: 0, estado: string, ciudad:string, calle: string, ext: string, int: string, referencia: string, default: string, activa: string} = {    
      id: 0,
      estado: '',
      ciudad: '',
      calle: '',
      ext: '',
      int: '',
      referencia: '',
      default: '',
      activa: ''      
  };
  account: { estado: 0, id: 0, name: string, email:string, birthday: string, gender: string, phone: string, password: string, terms: 0} = {
      estado: 0,
      id: 0,
      name: '',
      email: '',
      birthday: '',
      gender: '',
      phone: '',
      password: '*****',
      terms: 0
  };
  direcciones: boolean = false;
  pedidos: boolean = false;
  datos: boolean = false;
  sucursal : any;
  sucursales : any;
  pedidosactuales: any;
  token: any;
  loader: any = null;
  updateAddressForm: FormGroup;
  updateUserForm: FormGroup;
  constructor(public alert: AlertService, private service:EnvService, private storage: Storage, private toastController: ToastController) { 
  	Promise.all([this.storage.get('sucursal_seleccionada'), this.storage.get('name'), this.storage.get('email'), this.storage.get('fechanacimiento')
  		, this.storage.get('genero'), this.storage.get('celular'), this.storage.get('sucursales'), this.storage.get('sucursal'), 
  		this.storage.get('token'), this.storage.get('id')]).then(values => {
      this.account.name = values[1];
      this.account.email = values[2];
      this.account.birthday = values[3];
      this.account.gender = values[4];
      this.account.phone = values[5];
      this.sucursales = values[6]
      this.sucursal = values[7];
      this.token = values[8];
      this.account.id = values[9];
      console.log(this.account);  
      console.log(this.sucursal);   
      console.log(this.sucursales);
      console.log(this.account.phone);
      this.requestData(this.account.id);

	});
  	
  }

  ngOnInit() {
  	this.updateUserForm = new FormGroup({
      nameValidator: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*')]),
      emailValidator: new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
      birthdayValidator: new FormControl('', [Validators.required]),
      genderValidator: new FormControl('',[Validators.required, Validators.pattern('[a-zA-Z ]*')]),
      phoneValidator: new FormControl('',[Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(10),  Validators.maxLength(10)]),
      passwordValidator: new FormControl('', [Validators.required, Validators.minLength(5)])
    });

    this.updateAddressForm = new FormGroup({
      estadoValidator: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*')]),
      ciudadValidator: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*')]),
      coloniaValidator: new FormControl('',  [Validators.required, Validators.pattern('[a-zA-Z ]*')]),
      calleValidator: new FormControl('',  [Validators.required, Validators.pattern('[a-zA-Z ]*')]),
      extValidator: new FormControl('',[Validators.required]),
      intValidator: new FormControl('',[Validators.required]),
      referenciaValidator: new FormControl('',[Validators.required])
      
    });

   
  }

  updateUser(){
  	//console.log(this.account.birthday.substr(0,10));
  	if (this.account.password == "*****"){
  	  let params = {
        token: this.token,
        id: this.account.id,
        name: this.account.name,
        email: this.account.email,
        celular:this.account.phone,      
        fechanacimiento: this.account.birthday.substr(0,10),
        genero:this.account.gender,
        foto: '',
        password: '',
        level_access: 'USUARIOAPP'
      }
    //console.log(params);
    this.service.put_token('usuario', params, this.token)    
      .subscribe(
       (data) => {        
          //console.log(data);
          this.presentToast('¡Datos actualizados exitosamente!');
          let datos = data['data'];
          this.storage.set('loginSession', 'true');
          this.storage.set('token', datos.token);
          this.storage.set('name', datos.name);
          this.storage.set('email', datos.email);
          this.storage.set('date', datos.fechanacimiento);
          this.storage.set('phone', datos.celular);
          this.storage.set('genero', datos.genero);
          this.storage.set('foto', datos.foto);
          this.storage.set('id', datos.id);
          //console.log('Datos Actualizados');
          //console.log(datos);
         },
         (err) => {
           let error = err.error;
           //console.error(error.message);
           this.presentToast('¡Ocurrio un error al actualizar los datos!');
       });
	} else {
		let params = {		  
  		  token: this.token,
  		  id: this.account.id,
	      name: this.account.name,
	      email: this.account.email,
	      celular:this.account.phone,
	      fechanacimiento: this.account.birthday.substr(0,10)+' 00:00:00',
	      genero:this.account.gender,
	      foto: 'https://picsum.photos/100/100',
	      password: this.account.password,
	      level_access: 'USUARIOAPP'
	    }
	    //console.log(params);
      this.service.put_token('usuario', params, this.token)    
      .subscribe(
       (data) => {
        
        	//console.log(data);
        	this.presentToast('¡Datos actualizados exitosamente!');
        	let datos = data['data'];
	        this.storage.set('loginSession', 'true');
	        this.storage.set('token', datos.token);
	        this.storage.set('name', datos.name);
	        this.storage.set('email', datos.email);
	        this.storage.set('date', datos.fechanacimiento);
	        this.storage.set('phone', datos.celular);
	        this.storage.set('genero', datos.genero);
	        this.storage.set('foto', datos.foto );
	        this.storage.set('id', datos.id);
	        //console.log('Datos Actualizados');
	        //console.log(datos);
         },
         (err) => {
           let error = err.error;
           console.error(error.message);
           this.presentToast('¡Ocurrio un error al actualizar los datos!');
       });
	  }
 	}

  requestData(idAccount){
    console.log(idAccount);
    let params = idAccount;
    this.storage.get('token').then((token) => {
    this.service.simple_get_token('perfil', params, token)
     .pipe(map(data => {    
         //console.log(data['msg']); 
         console.log(data);
        this.pedidosactuales = data['pedidosactuales'];
        console.log(data['pedidosactuales']); 
        console.log(data['direcciones']); 
        this.address.estado = "Veracruz";
        this.address.ciudad = "Coatzacoalcos";
     }))     
     .subscribe(
      (data) => {       
           
      },
      (err) => {
         console.log(err);
         let error = err['error'].msg;
         console.error(error);
         
      });
   });

  }

 	async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      position: 'top',
    });
    toast.present();
  }

  updateAddress(){

  }

  mostrarPedidos(){
     !this.pedidos;
  }

  mostrarDirecciones(){
    !this.direcciones;
  }

   mostrarDatos(){
    !this.datos;
  }
}
