import { Component, OnInit } from '@angular/core';
import { MenuController, NavController, Platform, ToastController, LoadingController } from '@ionic/angular';
import { Router } from  "@angular/router";
import { Storage } from '@ionic/storage';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { map } from 'rxjs/operators';

//Services
import { AuthenticationService } from 'src/app/services/authentication.service';
import { AlertService } from 'src/app/services/alert.service';
import { EnvService } from 'src/app/services/env.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  user = { email:'', password:''}
  
  loginUserForm: FormGroup;
  loaderToShow: any;

  constructor(public navCtrl: NavController,  public platform: Platform, public loadingController: LoadingController, public menu: MenuController,
    private  router:  Router, private storage: Storage, private authService: AuthenticationService, private service: EnvService, 
    public alert: AlertService, public toastController: ToastController ) {

  }

  ngOnInit() {
    this.menu.enable(false);
    this.loginUserForm = new FormGroup({     
      emailValidator: new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),    
      passwordValidator: new FormControl('', [Validators.required, Validators.minLength(5)])
    });
  }

  login(){
    let params = {
      email: this.user.email,
      password: this.user.password,
      device_name: 'app'
    }
    this.showLoader();
    
      this.service.post( 'login', params )
      .pipe(map(data => {     
            this.hideLoader();      

            let datos = data['data'];
            console.log(datos);
            let login = this.authService.login('true', datos.token, datos.name, datos.email, datos.fechanacimiento, datos.celular, datos.genero, datos.foto, datos.id, datos.email_verified_at);  
                        
       }))  
      .subscribe(
        (data) => {
         
        },
        (err) => {
          this.hideLoader();
          let error = err.error;
          if(error.message == 'Unauthorised.'){
            this.presentToast('Usuario o contraseña incorrecto');
           
          } else {
            
            this.presentToast('De momento no contamos con sistema, pronto estaremos en línea');
            this.hideLoader();
           
          }
        }
      )
   

  }

  hideShowPassword() {
     this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
     this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  requestUserData(token, id){
    //console.log(token, id);
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  forgot(){
    this.navCtrl.navigateRoot('recover');
  }

  showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: 'Cargando datos...'
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        
      });
    });    
  }

  hideLoader() {
      this.loadingController.dismiss();
  }
}
