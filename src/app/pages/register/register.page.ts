import { Component, OnInit } from '@angular/core';
import { MenuController, NavController, Platform, ToastController, LoadingController } from '@ionic/angular';
import { Router } from  "@angular/router";
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { map } from 'rxjs/operators';

//Services
import { EnvService } from 'src/app/services/env.service';
import { AlertService } from 'src/app/services/alert.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  saveUserForm: FormGroup;
  account: { estado: 0, name: string, email:string, birthday: string, gender: string, phone: string, password: string, terms: 0} = {
      estado: 0,
      name: '',
      email: '',
      birthday: '',
      gender: '',
      phone: '',
      password: '',
      terms: 0
  };
  loaderToShow: any;
  private myToast: any;

  constructor(public navCtrl: NavController,  public platform: Platform, public loadingController: LoadingController, public menu: MenuController,
              private  router: Router, public alert: AlertService, private service:EnvService, private storage: Storage, 
              private authService: AuthenticationService, public toastController: ToastController) {

  }


  ngOnInit() {
  	 this.saveUserForm = new FormGroup({
      nameValidator: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*')]),
      emailValidator: new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
      birthdayValidator: new FormControl('', [Validators.required]),
      genderValidator: new FormControl('',[Validators.required, Validators.pattern('[a-zA-Z ]*')]),
      phoneValidator: new FormControl('',[Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(10),  Validators.maxLength(10)]),
      passwordValidator: new FormControl('', [Validators.required, Validators.minLength(5)])

    });
  }

  ionViewDidEnter() {
  	this.menu.enable(false);
  }

  async saveUser(){
    
    if(this.saveUserForm.valid){
    this.showLoader();
      //VALIDAR EL FORMULARIO ANTES DE ENVIAR
    //console.log(this.account.birthday.substr(0,10));
    let params = {
      name: this.account.name,
      email: this.account.email,
      celular:this.account.phone,
      fechanacimiento: this.account.birthday.substr(0,10),
      genero:this.account.gender,
      password: this.account.password,
      c_password: this.account.password,
      level_access: 'USUARIOAPP'
    }
    //console.log(params);
   
   await this.service.post( 'registrar', params )
    
    .pipe(map(data => {     
        
        this.hideLoader();   
        let datos = data['data'];
        let login = this.authService.register('true', datos.token, datos.name, datos.email, datos.fechanacimiento, datos.celular, datos.genero, datos.foto, datos.id, datos.email_verified_at);  
             
    }))
    .subscribe(
      (data) => {

      },
      (err) => {
        console.log(err['error']);
        let error = err['error'].error;       
        this.hideLoader();
          //CACHAR ERROR DE REGISTRO DUPLICADO
          this.alert.show('Alerta','Mensaje',error);
        
    });

      
    } else {
      this.alert.show('Advertencia','Mensaje','¡Registro incompleto! Por favor llene todos los datos')
    }
    
     
  
  }

  hideShowPassword() {
     this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
     this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: 'Cargando datos...'
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        console.log('Loading dismissed!');
      });
    });    
  }

  hideLoader() {
      this.loadingController.dismiss();
  }

  showToast(message) {
    this.myToast = this.toastController.create({
      message: message,
      duration: 2000
    }).then((toastData) => {
      //console.log(toastData);
      toastData.present();
    });
  }


  showDate(value){
    //console.log(value);
  }


}
