import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { RecoverPage } from './recover.page';
import { ComponentsModule } from './../../components/components.module';

const routes: Routes = [
  {
    path: '',
    component: RecoverPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    FormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],exports: [], providers: [],
  declarations: [RecoverPage]
})
export class RecoverPageModule {}
