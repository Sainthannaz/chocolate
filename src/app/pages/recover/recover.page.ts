import { Component, OnInit } from '@angular/core';
import { MenuController, NavController, Platform, ToastController, LoadingController, IonSlides, DomController, ModalController } from '@ionic/angular';
import { Router } from  "@angular/router";
import { Storage } from '@ionic/storage';
import { map } from 'rxjs/operators';
import { EnvService } from 'src/app/services/env.service';

@Component({
  selector: 'app-recover',
  templateUrl: './recover.page.html',
  styleUrls: ['./recover.page.scss'],
})
export class RecoverPage implements OnInit {
  user : { email:String, password: String, pin:String} = { email:'', password:'', pin:'' }
  showEmail = true;
  showPin = false;
  showNewPassword = false;
  token: any;
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  constructor(public navCtrl: NavController, public modelCtrl: ModalController, private router: Router, public loadingCtrl: LoadingController, 
    private storage: Storage, private service:EnvService, private toastCtrl:ToastController) { }

  ngOnInit() {
    this.storage.get('token').then((result) => { 
      this.token = result;
    });
  }

  Pin: String ="";
 
 
  forgot(){
    this.showPin = true;
    this.showEmail = false;
    this.showNewPassword = false;
    //console.log(this.user.email);
   
    this.service.simple_get('recuperarpassword', this.user.email)
       .pipe(map(data => {    
           console.log(data['msg']);         
           this.showToast(data['msg']);

       }))     
       .subscribe(
        (data) => {       
             
        },
        (err) => {
           console.log(err);
           let error = err['error'].msg;
           console.error(error);
           this.showToast(error);
        });

  }

  eventCapture(event) {
    this.showPin = false;
    this.showNewPassword = true;  
    this.Pin=event;
    //console.log(this.Pin);
    this.user.pin = this.Pin;
    //console.log(this.user.email);
    //console.log(this.user.pin);

    
  }

  async showToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  savePassword(){
    //console.log(this.user.email);
    //console.log(this.user.pin);
    //console.log(this.user.password);
    let params = {   
      email: this.user.email,
      password: this.user.password,
      codigo: this.user.pin
    }
    this.service.post('cambiarpassword', params)
       .pipe(map(data => {    
           //console.log(data['msg']);         
           this.showToast(data['msg']);
           this.navCtrl.navigateRoot('dashboard');
       }))     
       .subscribe(
        (data) => {       
           this.navCtrl.navigateRoot('dashboard');  
        },
        (err) => {
           console.log(err);
           let error = err['msg'];
           console.error(error);
           this.showToast(error);
        });

  
  }

  hideShowPassword() {
   this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
   this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }
}
