import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule, AlertController  } from '@ionic/angular';
import { CartPage } from './cart.page';
import { CardPage } from '../../modals/card/card.page';


const routes: Routes = [
  {
    path: '',
    component: CartPage
  }
];

@NgModule({
  imports: [
    CommonModule,    
    FormsModule,    
    IonicModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: CartPage
      }
    ])
  ],
  declarations: [CartPage, CardPage],
  entryComponents: [CardPage]
})
export class CartPageModule {}
