import { Component, OnInit, NgZone } from '@angular/core';
import { AlertController, LoadingController, ToastController, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { CartService, Item } from '../../services/cart.service';
import { Platform, IonList } from '@ionic/angular';
import { EnvService } from 'src/app/services/env.service';
import { ViewChild } from '@angular/core';
//import { Stripe } from '@ionic-native/stripe/ngx';
import { HttpClient } from "@angular/common/http";
import { CardPage } from '../../modals/card/card.page';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import * as moment from 'moment';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
  alertText: any = {};
  items: Item[] = [];
  today: any ;
  minDate: any;
  maxDate: any;
  minTime: any;
  maxTime: any;
  fecha: any = "";
  hora: any = "";
  tipo_servicio: any = "0";
  newItem: Item = <Item>{};  
  mylist: IonList;
  totalPrice : number = 0;
  price : number;
  paymentAmount: string;
  currency: string = 'MXN';
  currencyIcon: string = '$';
  cardDetails: any = {};
  articles: any;
  articlesToBuy: any;
  date:any;  
  loaderToShow:any;
  sinArticulos: boolean = true;
  conArticulos: boolean = false;
  pickup: boolean = false;
  domicilio: boolean = false;
  latitude: any = 0; //latitude
  longitude: any = 0; //longitude
  colonia: any;
  calle: any;
  numero_ext: any;
  costo_envio: any;
  //arreglo: [{ "producto_id":number, "cantidad": string, "fecha_entrega": string, "precio":number, "total":number, "leyenda":string, "chispas":string, 
  //"statusproduccion":string, "statuspedido":string }];
  arreglo: any = [];
  account: { estado: 0, id: 0, name: string, email:string, birthday: string, gender: string, phone: string, password: string, terms: 0} = {
      estado: 0,
      id: 0,
      name: '',
      email: '',
      birthday: '',
      gender: '',
      phone: '',
      password: '*****',
      terms: 0
  };
  fechasparapedir: any = [];
  sucursal : any;
  sucursales : any;
  token: any;
  loader: any = null;

  constructor(  public toastController: ToastController, public loadingController: LoadingController, private translate: TranslateService,
    private storage: Storage, private router: Router, public alertCtrl: AlertController, private plt: Platform, private service:EnvService,
    private cartService: CartService, private http: HttpClient, public modalController: ModalController,  private geolocation: Geolocation ) { 
    this.plt.ready().then(() => {
      // this.fakeData();
      this.loadItems();      
      this.today = Date.now();
      Promise.all([this.storage.get('sucursal_seleccionada'), this.storage.get('name'), this.storage.get('email'), this.storage.get('date')
      , this.storage.get('genero'), this.storage.get('phone'), this.storage.get('sucursales'), this.storage.get('sucursal'), 
        this.storage.get('token'), this.storage.get('id'), this.storage.get('fecha_min_entrega'), this.storage.get('fecha_max_entrega')]).then(values => {
        this.account.name = values[1];
        this.account.email = values[2];
        this.account.birthday = values[3];
        this.account.gender = values[4];
        this.account.phone = values[5];
        this.sucursales = values[6]
        this.sucursal = values[7];
        this.token = values[8];
        this.account.id = values[9];       
        this.minTime = this.today;        
        console.log(this.account);  
        console.log(this.sucursal);   
        console.log(this.sucursales);
        this.requestDates();    
      });
    });
  }

  ionViewWillEnter(){
    //console.log(this.token);
    
  }

  options = {
    timeout: 10000, 
    enableHighAccuracy: true, 
    maximumAge: 3600
  };

  // use geolocation to get user's device coordinates
  getCurrentCoordinates() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
     }).catch((error) => {
       console.log('Error getting location', error);
     });
  }
  
  ngOnInit() {
	  	this.translate.get('ALERT_TITLE_CART').subscribe(title => {
	      this.alertText.title = title;
	    });
	    this.translate.get('ALERT_MESSAGE_CART').subscribe(message => {
	      this.alertText.message = message;
	    });
	    this.translate.get('ALERT_OK').subscribe(ok => {
	      this.alertText.ok = ok;
	    });
	    this.translate.get('ALERT_CANCEL').subscribe(cancel => {
	      this.alertText.cancel = cancel;
	    });
      this.translate.get('CART_ITEM_ADDED').subscribe(item => {
        this.alertText.item = item;
      });

  }

   async emptyCart() {
    //console.log('Alert Event');
    const alert = await this.alertCtrl.create({
      header: this.alertText.title,    
      message: this.alertText.message,
      buttons: [ {
          text: this.alertText.cancel,
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            //console.log('Confirm Cancel');
          }
        }, {
          text:  this.alertText.ok,
          handler: () => {
            //console.log('Confirm Ok');             
             this.deleteAll();
             this.loadItems();
          }
        }]
    });
    await alert.present();
  }

  // Agregar items, No se utiliza pero lo dejo como informativo.
  addItem() {  
      this.cartService.addItem(this.newItem).then(item => {
      this.newItem = <Item>{};
      this.showToast(this.alertText.item)
      this.loadItems(); // Or add it to the array directly
    });
  }
  
  // Actualizar, no se utiliza por el momento pero se agrega como información de uso

   updateItem(item: Item) {
    item.descripcion = `UPDATED: ${item.descripcion}`;
    item.fecha_entrega = "";

    this.cartService.updateItem(item).then(item => {
      this.showToast('Item updated!');
      this.mylist.closeSlidingItems();
      //this.mylist.closeSlidingItems(); // Fix or sliding is stuck afterwards
      this.loadItems(); // Or update it inside the array directly

    });
  }

  // Elimina un articulo del carrito de compras
  deleteItem( item: Item, i) { 
    //console.log(item, i);
    this.cartService.deleteItem(item.id).then(item => {
      this.showToast('Se elimina el pastel!');
      //this.mylist.closeSlidingItems(); // Fix or sliding is stuck afterwards
      this.loadItems(); // Or splice it from the array directly
    }); 
  }
  

  deleteAll () {
    this.cartService.deleteAll();
  }
  
  // Se leen todos los articulos
  async loadItems() {
    await this.cartService.getItems().then(items => {
      this.items = items;
      //console.log('Items a comprar');     
      //console.log(this.items);  
      this.articlesToBuy = this.items.length
      //console.log(this.items.length);
    });   
     
     this.calculateTotal();  
     //console.log(this.arreglo);
  }
  
  // Toast´s
  async showToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  fakeData(){
    this.addItem(); 
  }

  // Calculo del total a pagar

  calculateTotal(){
    this.totalPrice = 0;   
    //console.log('calcular total');    
      this.items.forEach( (item, index) => {  
        //let fecha = moment(this.fecha).format('YYYY-MM-DD');
        //console.log(this.fecha);
        //console.log(fecha);
        this.price = Number(item.precio);
        this.totalPrice += this.price;
        /*this.arreglo.push({producto_id:item.producto_id, cantidad:item.cantidad, fecha_entrega:"2020-12-31", precio:item.precio, total:item.precio, 
          leyenda:item.leyenda, chispas:item.chispas, statusproduccion: "en espera de producción", statuspedido:"es espera de pedido"});*/
        //console.log(this.totalPrice);     
      });
      console.log(this.totalPrice);
      if (this.totalPrice == 0) {
        this.sinArticulos = true;
        this.conArticulos = false;
      } else {
        this.sinArticulos = false;
        this.conArticulos = true;
      }
    //console.log('total: '+ this.totalPrice);
    //console.log(this.articlesToBuy);
  }
  
  async getCardData() {
    let val = 0;
    
     if (this.articlesToBuy >= 1 ){   
       console.log(this.fecha);
       console.log(this.hora);
       console.log(this.tipo_servicio);

         if ( this.fecha == "" || null){       
           this.showToast('¡Selecciona la fecha en que pasas por tu pastel!');       
           val = 1;     
         } else { val = 0;} 

         if ( this.hora == "" || null){       
           this.showToast('¡Selecciona la hora en que pasas por tu pastel!');       
           val = 1;     
         } else { val = 0;} 

         if ( this.tipo_servicio === "0" || null){       
           this.showToast('¡Selecciona si vienes por el pastel o te lo enviamos!');       
           val = 1; 

           if (this.colonia == "") {

           }  else {}

           if (this.calle == "") {

           }  else {}

           if (this.numero_ext == "") {

           }  else {}


         } else { val = 0;} 

       console.log(this.tipo_servicio);

         if (val == 0){
           console.log(val);
           console.log('Paso por acá');
            this.showLoader();
            this.items.forEach( (item, index) => {  
            let fecha = moment(this.fecha).format('YYYY-MM-DD');
            //console.log(this.fecha);
            //console.log(fecha);
            
            this.arreglo.push({producto_id:item.producto_id, cantidad:item.cantidad, fecha_entrega:fecha, precio:item.precio, total:item.precio, 
              leyenda:item.leyenda, chispas:item.chispas, statusproduccion: "en espera de producción", statuspedido:"es espera de pedido", Peso:item.peso, Pan:item.pan, Relle:item.relle, Cubi:item.cubi, descripcion:item.descripcion });
            //console.log(this.totalPrice);     
          });

           let params = {       
                 idcliente: this.account.id,

                 idsucursal: this.sucursal,
                 fecha_pedido: moment(Date.now()).format('YYYY-MM-DD'),
                 hora_pedido: moment(Date.now()).format('HH:mm:ss'),
                 fecha_entrega: moment(this.fecha).format('YYYY-MM-DD'),
                 hora_entrega: moment(this.hora).format('HH:mm:ss'),           
                 total: this.totalPrice,                    
                 productos: this.arreglo
                      
            }   
            console.log(params);
            //this.storage.set('pedido', params);
            await this.storage.get('token').then((token) => {
             this.service.post_token('pedidos', params, token)
             .pipe(map(data => {     
               //console.log(data['data']);
               let datos = data['data'];
               this.storage.set('pedido', datos);
              
               this.hideLoader();
               
             }))     
             .subscribe(
               (data) => {
                   
                    this.makePay(); 

                 },
                 (err) => {
                   this.hideLoader();
                   //console.log(err);
                   let error = err.error;
                   //console.error(error.error);
                   this.showToast(error.error);
                   //this.verifyUser();
               });
             });
         }
       } else {
        this.showToast('El carrito no cuenta con articulos');
      }
  
   
    /*
    
    */

  }

  async makePay(){
    const modal = await this.modalController.create({
      component: CardPage,
      componentProps: {
        payAmount: this.totalPrice
      }

    });
    return await modal.present().then(_ => {
    // triggered when opening the modal
    //console.log('Sending: ', this.totalPrice);
    });
  }
 
   showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: 'Validando pedido...'
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        //console.log('Loading dismissed!');
      });
    });
   
  }

   hideLoader() {
      this.loadingController.dismiss();
  }

  async verifyUser(){
    //console.log('Alert Event');
    const alert = await this.alertCtrl.create({
      header: "Verificar usuario",    
      message: "Por favor, por la seguridad de las transacciones es necesario verificar su usuario",
      buttons: [ {
          text: this.alertText.cancel,
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            //console.log('Confirm Cancel');
            this.showToast('Hasta no verificar usuario no podra realizar compras dentro de la aplicación');
          }
        }, {
          text:  this.alertText.ok,
          handler: () => {
            //console.log('Confirm Ok');             
            this.router.navigateByUrl('verify');    
          }
        }]
    });

    await alert.present();
  }

  requestDates(){
     this.service.get_token('pedidos/factible',{}, this.token)
     .pipe(map(data => {     
       //console.log(data['fechasparapedir']);  
        const arr = data['fechasparapedir'];
        //console.log(this.fechasparapedir.length);
        var min = arr.reduce(function (valor1, valor2) { return new Date(valor1) <  new Date(valor2) ? valor1 : valor2; }); 
        var max = arr.reduce(function (valor1, valor2) { return new Date(valor1) > new Date(valor2) ? valor1 : valor2; });
  
        /*
        let arrayFechas = this.fechasparapedir.map((fechaActual) => new Date(fechaActual) );

        var max = new Date(Math.max.apply(null,arrayFechas));
        var min = new Date(Math.min.apply(null,arrayFechas));
        */
        //console.log("valor minimo" , min) 
        //console.log("valor maximo" ,max)
        this.minDate = min.substring(0,10);
        this.maxDate = max.substring(0,10);
          
        /*
        if (this.fechasparapedir.length == null){

        } else if (this.fechasparapedir.length == 1){
          this.minDate = this.fechasparapedir[0].substring(0,10);
          this.maxDate = this.fechasparapedir[0].substring(0,10);
          console.log(this.minDate);
          console.log(this.maxDate);
        } else if (this.fechasparapedir.length == 2){
          this.minDate = this.fechasparapedir[0].substring(0,10);
          this.maxDate = this.fechasparapedir[1].substring(0,10);
          console.log(this.minDate);
          console.log(this.maxDate);
        } else if (this.fechasparapedir.length == 3){
          this.minDate = this.fechasparapedir[0].substring(0,10);
          this.maxDate = this.fechasparapedir[2].substring(0,10);
          console.log(this.minDate);
          console.log(this.maxDate);
        }
        */
        //this.minDate = moment().add(12, 'hours').format('YYYY-MM-DD');
        //this.maxDate = moment().endOf('month').format('YYYY-MM-DD')
     }))     
     .subscribe(
       (data) => {
                 

         },
         (err) => {
           let error = err.error;
           //console.error(error.message);
       });
  }

  checkServicio(event){
   let val :any = event.target.value;
   console.log(val);
   if (val == 'pickup') {
     console.log("pickUp");
     this.pickup = true;
     this.domicilio = false;
   } if (val == 'domicilio'){
     console.log("domicilio");
     this.pickup = false;
     this.domicilio = true;
   }
   //console.log(this.cakeData.mensaje);
  }
}
