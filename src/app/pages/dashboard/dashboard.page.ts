import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MenuController, NavController, Platform, ToastController, LoadingController, AlertController } from '@ionic/angular';
import { Router } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { CartService, Item } from '../../services/cart.service';
import { Cakes } from "../../mocks/providers/cakes";
import { Cake } from "../../models/cake";
import { AuthenticationService } from './../../services/authentication.service';
import { map } from 'rxjs/operators';
import { EnvService } from 'src/app/services/env.service';
import { ImageLoaderConfigService, ImageLoaderService } from 'ionic-image-loader';

import * as moment from 'moment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})


export class DashboardPage implements OnInit {

  @ViewChild('slider', { read: ElementRef })slider: ElementRef;
  img: any;
  cart = [];
  myBranchs = [];
  fakeData:any;
  currentCakes: any = [];
  someTestCakes:any = [];
  currentDetails: any = [];
  items: Item[] = [];
  cartItems:any;
  filterCakes: Cake[];
  datosCake: any = [];
  sucursales: any = [];
  catalogos: any = [];
  sliderOpts = {
    autoplay: true,
    zoom: {
      maxRatio: 5
    }
  };
  alertText: any = {};
  constructor(public navCtrl: NavController,  public platform: Platform, public loadingCtrl: LoadingController,
   public menu: MenuController, private translate: TranslateService, private storage: Storage, private router: Router,
   public alertCtrl: AlertController, private cartService: CartService, public cakes: Cakes, private authenticationService: AuthenticationService,
   private service:EnvService, private imageLoaderConfig: ImageLoaderConfigService) {
    this.platform.backButton.subscribeWithPriority(0, () => {
     this.presentAlert();
    });

    //this.imageLoaderConfig.setFallbackUrl('assets/imgs/fallback.png');   
    this.imageLoaderConfig.enableSpinner(true);
    //this.imageLoaderConfig.enableFallbackAsPlaceholder(true);
    this.imageLoaderConfig.setMaximumCacheAge(7 * 24 * 60 * 60 * 1000); // 7 días para almacenar las imagenes en cache    
  }

  ngOnInit() {
    this.translate.get('ALERT_TITLE_EXIT').subscribe(title => {
      this.alertText.title = title;
    });
    this.translate.get('ALERT_MESSAGE_EXIT').subscribe(message => {
      this.alertText.message = message;
    });
    this.translate.get('ALERT_OK').subscribe(ok => {
      this.alertText.ok = ok;
    });
    this.translate.get('ALERT_CANCEL').subscribe(cancel => {
      this.alertText.cancel = cancel;
    });
    this.translate.get('CART_ITEM_ADDED').subscribe(item => {
        this.alertText.item = item;
    });  
    
    //console.log('Solicitando sucursales');       
    this.storage.get('sucursal_seleccionada').then((result) => { 
      //console.log('El estado de la sucursal es: ', result);
      console.log(result);
      if (result){
        //console.log("true");                
        console.log("sucursal seleccionada");        
      }
      if (result != true){
        console.log("sucursal no seleccionada");        
        this.requestBranch();
        //this.presentAlertRadio();       
      }     
    });

    

   this.loadData();
   //this.loadProducts();

    //Solicitamos catalogos de productos
    //this.requestCatalogs();
    // Solicitamos los productos a vender    
    this.filterCakes = this.cakes.query();
    this.currentCakes = this.filterCakes;
  }

  ionViewDidLoad() {
    //this.loadData();
  }

  ionViewDidEnter() {
    this.authenticationService.authenticationState.subscribe(state => {
      if (state) {
        //console.log(state);
        this.router.navigateByUrl('dashboard');
      } else {
        //console.log(state);
        this.router.navigateByUrl('home');
      }
    });
	  this.menu.enable(true);
    //console.log('Cargando datos...');
    this.loadData();
  }

 openCart(){
    this.router.navigateByUrl('cart');
 }

 openItem(cake){
   console.log("datos del pasteles");
   console.log(cake);
    this.router.navigate(['/detail', cake]);
 }

 loadData(){
   this.loadItems();
   //this.cakes.deleteAll();   
   console.log(this.cakes);
    this.storage.get('pasteles_locales').then((result) => { 
        //console.log(result);
        for (let i = 0; i < result.length; i++) {
            //console.log(result[i]);
            this.cakes.add(result[i]);
        } 

    });
 }
 
 loadProducts(){
   let params = {       
    
    }
   this.storage.get('token').then((token) => {
    this.service.get('productos/sg', params, token)
     .pipe(map(data => {    
         console.log(data['msg']); 
         console.log(data);         
         this.someTestCakes = data;
     }))     
     .subscribe(
      (data) => {       
           
      },
      (err) => {
         console.log(err);
         let error = err['error'].msg;
         console.error(error);
         
      });
   });

 }
 

 requestBranch(){
   //console.log('Solicitaremos las sucursales');
   this.storage.get('token').then((token) => {
     this.service.get_token('sucursales',{}, token)
     .pipe(map(data => {     
       this.sucursales = data['data'];        
       //console.log(this.sucursales);      
     }))     
     .subscribe(
       (data) => {        
         //console.log(this.sucursales);
         this.sucursales.forEach((el,idx) => {
          //console.log(el.nombresucursal);
          this.storage.set('sucursales', this.sucursales); 
         }); 
          //console.log('Rellenando las sucursales: ');
          this.myBranchs = this.populateBranchs();
          //console.log(this.myBranchs);
          this.presentAlertRadio();
          
      },
         (err) => {
           let error = err.error;
           console.error(error.message);
       });
     });
 }

 requestCatalogs(){
   //console.log('Solicitaremos catalogos');
   this.storage.get('token').then((token) => {
     this.service.get_token('catalogos',{}, token)
     .pipe(map(data => {     
       this.catalogos = data;        
       //console.log(this.catalogos);      
     }))     
     .subscribe(
       (data) => {        
         
         }); 
         
      },
         (err) => {
           let error = err.error;
           //console.error(error.message);
       });
     
 }

 getCakes(ev) {
    ////console.log(ev);
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.currentCakes = this.filterCakes;
      return;
    }
    this.currentCakes = this.cakes.query({
      name: val
    });
  }

 async presentAlert() {
    //console.log('Alert Event');
    const alert = await this.alertCtrl.create({
      header: this.alertText.title,
      cssClass: 'exitAlert',
      message: this.alertText.message,
      buttons: [ {
          text: this.alertText.cancel,
          role: 'cancel',
          cssClass: 'exitAlert',
          handler: () => {
            //console.log('Confirm Cancel');
          }
        }, {
          text:  this.alertText.ok,
          cssClass: 'exitAlert',
          handler: () => {
            //console.log('Confirm Ok');
             this.storage.set('loginSession', 'false');
             this.authenticationService.logout();
             this.router.navigateByUrl('home');
          }
        }]
    });
    await alert.present();
  }


  async presentAlertRadio() {
    const alert = await this.alertCtrl.create({
      header: 'Selecciona tu sucursal mas cercana',
      inputs: this.myBranchs,
      backdropDismiss: false,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            //console.log('Confirm Cancel');
            
          }
        }, {
          text: 'Ok',
          handler: (data:string) => {
            //console.log(data); 
            this.storage.set('sucursal', data);
            this.storage.set('sucursal_seleccionada', true);            
            //console.log('Confirm Ok');
          }
        }
      ]
    });

    await alert.present();
  }

  populateBranchs() {
    //console.log('Entramos a insertar la data de las sucursales...');
    const theNewInputs = [];
    //console.log(this.sucursales);
    for (let i = 0; i < this.sucursales.length; i++) {
      //console.log(this.sucursales[i].nombresucursal);
      if (this.sucursales[i].visibleenapp == 1){
        theNewInputs.push(
          {
            text: this.sucursales[i].nombresucursal,          
            type: 'radio',
            label:  this.sucursales[i].nombresucursal,   
            value: this.sucursales[i].id,
            checked: true
          }          
        );
      }
    }
    return theNewInputs;
  }  

  async loadItems() {
    await this.cartService.getItems().then(items => {
      this.items = items;
      //console.log('Items a comprar');     
      //console.log(this.items);      
      if (this.items == null){
        this.cartItems ="";
      } else {
        this.cartItems =  this.items.length;
      }
    },  (err) => { console.log(err);} ); 
  }
}
