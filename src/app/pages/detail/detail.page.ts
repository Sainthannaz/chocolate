import { Component, OnInit, ViewChild, ElementRef, Directive, Input, Renderer2, SimpleChanges} from '@angular/core';
import { MenuController, NavController, Platform, ToastController, LoadingController, IonSlides, DomController, AlertController  } from '@ionic/angular';
import { ActivatedRoute} from '@angular/router';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ScrollDetail,  } from '@ionic/core';
import { Router } from  "@angular/router";
import { CartService, Item } from '../../services/cart.service';
import { EnvService } from 'src/app/services/env.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  @ViewChild(IonSlides) slides: IonSlides;
  numero_pasteles: any;
  caracteres_restan : any;
  showToolbar = false;
  tipo: boolean;
  value : any;  
  val:any;
  today: any ;
  tamanoCarro:any;
  loaderToShow: any;
  personalizar: boolean = false;
  ingredientes :  any[] = [{
      value: 1,
      text: 'Chispas de chocolate'
    }, {
      value: 2,
      text: 'Chispas de colores'
    }];
  cake: any = [];

  cakeData : { id: 0, estado: 0, numero: 0, pan: string, relleno: string, cubierta: string, mensaje: string, tamano: string, ingredientes: string, descripcion:string } = {
    id: 0,
    estado: 0,
    numero: 0, 
    pan: '',
    relleno: '',
    cubierta: '',
    mensaje: '',
    tamano: '',
    ingredientes: '', 
    descripcion: ''
  };
  legend : any;
  legends : any = [] = [{
      value: 1,
      text: 'Muchas felicidades'
  },{
      value: 2,
      text: 'Sorpresa'
  },{
      value: 3,
      text: 'Feliz cumpleaños'
  },{
      value: 4,
      text: 'Te amo'
  },{
      value: 5,
      text: 'Te quiero'
  },{
      value: 6,
      text: 'Ninguna'
  }];

  /*
  tamanos : any =[] = [{
      id: 1,
      value: '1.8',      
      text: '1/8 Kg',
      description:'8 - 10'
    },{
      id: 2,
      value: '1.4',
      text: '1/4 Kg',
      description:'15 - 20'
    }];
  */
   
  reach: any;
   
  items: Item[] = [];
  newItem: Item = <Item>{};
  next: boolean;
  cart: boolean;
  alertText: any = {};

  constructor(public navCtrl: NavController,  private route: ActivatedRoute, public platform: Platform, public loadingController: LoadingController, private translate: TranslateService,
    public menu: MenuController, private  router:  Router, private cartService: CartService, public toastCtrl: ToastController, 
    public alertCtrl: AlertController, private service:EnvService, private storage: Storage) {    
    this.cake.id = this.route.snapshot.paramMap.get('IdProducto');  
    this.cake.idProducto = this.route.snapshot.paramMap.get('IdProducto');    
    this.cake.name = this.route.snapshot.paramMap.get('name');
    this.cake.description = this.route.snapshot.paramMap.get('description');    
    this.cake.peso = this.route.snapshot.paramMap.get('peso');
    console.log(this.cake.peso);
    if (this.cake.peso == "1/8") {
      this.reach = "8 - 10"
      this.cakeData.tamano = "1/8";
      this.tipo = true;
    } if (this.cake.peso == "1/4") {
      this.reach = "15 - 20"
      this.cakeData.tamano = "1/4";
      this.tipo = true;
    } if (this.cake.peso == "PZA") {
      this.reach = "1"
      this.cakeData.tamano = "PZA";
      this.tipo = false;
      this.cakeData.mensaje ="N/A";
     this.cakeData.ingredientes ="N/A";
    }

    this.cakeData.pan = this.route.snapshot.paramMap.get('pan');
    this.cakeData.relleno = this.route.snapshot.paramMap.get('relleno');
    this.cakeData.cubierta = this.route.snapshot.paramMap.get('cubierta');
    this.cakeData.descripcion =  this.route.snapshot.paramMap.get('name');

    //console.log(this.cakeData.descripcion);
    this.cake.price = this.route.snapshot.paramMap.get('price');
    //console.log(typeof(this.cake.price));
    this.cake.image = this.route.snapshot.paramMap.get('image');
    //console.log(this.cake.name);

  }

  ngOnInit() {
    this.translate.get('CART_TITLE_ADD').subscribe(title => {
      this.alertText.title = title;
    });

    this.translate.get('CART_MESSAGE_DETAIL').subscribe(message => {
      this.alertText.message = message;
    });

    this.translate.get('ALERT_OK').subscribe(ok => {
      this.alertText.ok = ok;
    });

    this.translate.get('ALERT_CANCEL').subscribe(cancel => {
      this.alertText.cancel = cancel;
    });    
    this.translate.get('CART_ITEM_ADDED').subscribe(item => {
        this.alertText.item = item;
    });
  	this.numero_pasteles = 0;
    this.next = true;	
  }

  onScroll($event: CustomEvent<ScrollDetail>) {
    if ($event && $event.detail && $event.detail.scrollTop) {
      const scrollTop = $event.detail.scrollTop;
      this.showToolbar = scrollTop >= 225;
    }
  }

  openCart(){
    this.router.navigateByUrl('cart');
 }

 addToCart(){
    ////console.log('DATOS DEL PASTEL:' );   
    console.log(this.cakeData.tamano);     
    console.log(this.cakeData.mensaje);
    console.log(this.cakeData.ingredientes);
    //this.requestCartSize();
   
    let val1 = 0;
    let val2 = 0;
    let val3 = 0; 

   if ( this.cakeData.tamano === ""){
     //console.log(this.cakeData.tamano);     
     this.showToast('Te faltan seleccionar el tamaño!');
     val1 = 1;     
     //console.log(val1);
   } else { val1 = 0;} 

   if (this.cakeData.mensaje === "" ) {
       //console.log(this.cakeData.mensaje);
       this.showToast('Te faltan datos de la leyenda del pastel!');
       val2 = 1;
       //console.log(val2);
   } else { val2 = 0;} 

   if (this.cakeData.ingredientes === "" ){
     //console.log(this.cakeData.ingredientes);   
     this.showToast('Te faltan datos de ingredientes!');
      val3 = 1;
      //console.log(val3);
   } else { val3 = 0;} 
   //console.log('VALOR: ------->' + this.val);
   if (val1 == 0 && val2 == 0 && val3 == 0){
    //console.log(this.tamanoCarro);
    //this.cakeData.id = this.cake.id;
    console.log('id' + this.cake.id);
    console.log('tamaño' + this.cakeData.tamano);
    console.log('ingredientes' + this.cakeData.ingredientes);
    console.log('tipo de pastel ' + this.cakeData.descripcion);
    console.log('ID producto' + this.cake.id);
    //console.log('mensaje' + this.cakeData.mensaje);
    //console.log('CAKE DATA');
    //console.log(this.cakeData);

    this.newItem.id =  Date.now(); // ESTE ES EL ID DEL CARRITO, ES LOCAL
    this.newItem.producto_id = this.cake.id; // ESTE ES EL ID DEL PRODUCTO
    this.newItem.peso = this.cakeData.tamano;
    this.newItem.precio = this.cake.price;
    this.newItem.cantidad = '1';
    this.newItem.descripcion =  this.cakeData.descripcion;
    this.newItem.pan = this.cakeData.pan;
    this.newItem.relle = this.cakeData.relleno;
    this.newItem.cubi = this.cakeData.cubierta;
    this.newItem.leyenda = this.cakeData.mensaje;
    this.newItem.chispas = this.cakeData.ingredientes;
    this.newItem.fecha_entrega =  "";
    //console.log('NEW ITEM');
    //console.log(this.newItem);
    this.presentAlert();
   

   } else {
     //console.log('No agrega nada!');
     this.showToast('Te faltan datos del pastel!');
   }
   
 }

 addCake(){
 	this.numero_pasteles += 1;
 }

 updatePersonalize(){
    //console.log(this.personalizar);
    if (this.personalizar) {    
      this.legend = null;  
      this.cakeData.mensaje ="";
      this.caracteres_restan = 25;
    } else {      
      this.cakeData.mensaje ="";
    }
 }

 removeCake(){
 	if (this.numero_pasteles >= 1) {
 		this.numero_pasteles -= 1;
 	}
 }

 updateLegend(event){
   let val :number = event.target.value;
   this.cakeData.mensaje= this.legends[val-1].text;
   //console.log(this.cakeData.mensaje);
 }

/*
 changeDescription(event){
   //console.log(event);  
   let val :number = event.target.value;
   //console.log(val);   
   //this.cakeData.descripcion = this.tamanos[val-1].description;      
   this.value = this.tamanos[val-1].description;
   //console.log(this.value);
   
   for (let i = 0; i < this.tamanos.length; i++) {
      //console.log(this.tamanos[i]);
      if (this.tamanos[i].id == val){
          this.cakeData.tamano = this.tamanos[i].value
      }
   }
   this.requestCakeData(this.cakeData.tamano);
   //console.log(this.cakeData.tamano);  
 }
 */

 checkMessage(){
   //console.log(this.cakeData.mensaje.length);
   //console.log(25 - this.cakeData.mensaje.length)
   this.caracteres_restan = 25 - this.cakeData.mensaje.length;
 }

goNextSlide(){  
    this.slides.slideTo(2, 500);  
}

slideChanged() {   
  this.slides.getActiveIndex().then(index => {
     //console.log(index);
     if (index == 0 ) {
        this.next = true;
        this.cart = false;
     } 

     if (index == 1 ) {
        this.next = true;
        this.cart = false; 
     }

     if (index == 2 ) {
       this.next = false;
       this.cart = true;
     }
  });
}

// Helper
  async showToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

   async presentAlert() {
    //console.log('Alert Event');
    const alert = await this.alertCtrl.create({
      header: this.alertText.title,    
      message: "Usted agregara al carrito un pastel de " + this.cakeData.descripcion + " " + this. cakeData.tamano + "K, acompañado de "+  this.cakeData.ingredientes + " con la leyenda: " + this.cakeData.mensaje,
      buttons: [ {
          text: this.alertText.cancel,
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            //console.log('Confirm Cancel');
          }
        }, {
          text:  this.alertText.ok,
          handler: () => {
            //console.log('Confirm Ok');
            this.cartService.addItem(this.newItem).then(item => {
            this.newItem = <Item>{};
            this.showToast(this.alertText.item);    
          });
          

          //this.cakeData.length = 0; 
          this.legend = null;
          this.cakeData.mensaje ="";
          this.cakeData.ingredientes = null;
          this.cakeData.tamano = null;
          this.cakeData.pan = null;
          this.cakeData.relleno = null;
          this.cakeData.cubierta = null;
          this.router.navigateByUrl('dashboard');  
          }
        }]
    });
    await alert.present();
  }

  /*
  requestCakeData(tamano){
    this.showLoader();
    //console.log(tamano);
    this.storage.get('token').then((token) => {
    //console.log('productos/peso/'+ this.cakeData.tamano + '/pan/'+ this.cakeData.pan + '/relle/'+ this.cakeData.relleno + '/cubi/'+ this.cakeData.cubierta +'', {}, token);
     this.service.get('productos/peso/'+ this.cakeData.tamano + '/pan/'+ this.cakeData.pan + '/relle/'+ this.cakeData.relleno + '/cubi/'+ this.cakeData.cubierta +'', {}, token)
     .pipe(map(data => {     
        //console.log(data['producto']);
        this.cakeData.id = data['producto'][0].IdProducto;
        this.cake.price = data['producto'][0].precio;
        this.hideLoader();
     }))     
     .subscribe(
       (data) => {
        
      },
         (err) => {
           this.hideLoader();
           let error = err.error;
           console.error(error.message);
       });
     });
   }
*/

  async requestCartSize(){       

     await this.cartService.getItems().then(items => {
      this.items = items;
      ////console.log(this.items);
      if (this.items == null){
        this.tamanoCarro = 0;
        //console.log(this.tamanoCarro);
      } else {
        this.tamanoCarro = this.items.length;
        //console.log(this.tamanoCarro);
      }    
     
    });     
  }

  showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: 'Cargando datos...'
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        
      });
    });    
  }

  hideLoader() {
      this.loadingController.dismiss();
  }


}
