import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { MenuController, NavController, Platform, ToastController, LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Router } from  "@angular/router";

export interface Slide {
  title: string;
  description: string;
  image: string;
  background: string;
}

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.page.html',
  styleUrls: ['./tutorial.page.scss'],
})
export class TutorialPage implements OnInit {
  slides: Slide[];
  showSkip = true;
  tutorial: string = "";

  constructor(public navCtrl: NavController, public menu: MenuController, public platform: Platform, private storage: Storage, public translate: TranslateService,
              private  router:  Router) { }

  ngOnInit() {
    this.storage.get('tutorial').then((val) => {
      console.log('Estado del tutorial: ', val);
      this.tutorial = val;
    });
    this.translate.get(['TUTORIAL_SLIDE1_TITLE',
      'TUTORIAL_SLIDE1_DESCRIPTION',
      'TUTORIAL_SLIDE2_TITLE',
      'TUTORIAL_SLIDE2_DESCRIPTION',
      'TUTORIAL_SLIDE3_TITLE',
      'TUTORIAL_SLIDE3_DESCRIPTION',
    ]).subscribe(
      (values) => {
        console.log('Loaded values', values);
        this.slides = [
          {
            title: values.TUTORIAL_SLIDE1_TITLE,
            description: values.TUTORIAL_SLIDE1_DESCRIPTION,
            image: './../assets/logo.png',
            background: './../assets/imgs/blur.jpg',

          },
          {
            title: values.TUTORIAL_SLIDE2_TITLE,
            description: values.TUTORIAL_SLIDE2_DESCRIPTION,
            image: './../assets/imgs/ica-slidebox-img-1.png',
            background: './../assets/imgs/blur.jpg',
          },
          {
            title: values.TUTORIAL_SLIDE3_TITLE,
            description: values.TUTORIAL_SLIDE3_DESCRIPTION,
            image: './../assets/imgs/ica-slidebox-img-2.png',
            background: './../assets/imgs/blur.jpg',
          }
        ];
     
     }, error => {
      console.error(error);
    });
  }

  onSlideChangeStart(slider) {
    this.showSkip = !slider.isEnd();
  }

  ionViewDidEnter() {
    // bloqueamos el menu en esta actividad
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    // terminando el menu liberaremos el menu 
    this.menu.enable(false);
  }


   startApp() {
    console.log("Presionamos start o skip");
    this.storage.set('tutorial', 'true');
    this.storage.get('tutorial').then((val) => {
     console.log('Tutorial iniciado: ', val);
    });    
    this.router.navigateByUrl('home');  
   }

}
