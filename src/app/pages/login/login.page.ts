import { Component, OnInit } from '@angular/core';
import { MenuController, NavController, Platform, ToastController, LoadingController } from '@ionic/angular';
import { ScrollDetail } from '@ionic/core';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { Router } from  "@angular/router";
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  showToolbar = false;
  user: { email: string, first_name: string, id: string, last_name: string, name: string, picture: { data: { url: string } } };  
  constructor(public navCtrl: NavController,  public platform: Platform, public loadingCtrl: LoadingController, public menu: MenuController, private fb: Facebook,
    private  router:  Router) {

  }

  ngOnInit() {
    
  	
  }

  ionViewDidEnter() {	
  	this.menu.enable(false);
  }

  onScroll($event: CustomEvent<ScrollDetail>) {
    if ($event && $event.detail && $event.detail.scrollTop) {
      const scrollTop = $event.detail.scrollTop;
      this.showToolbar = scrollTop >= 225;
    }
  }

  onClickFacebook() {
    console.log('Ingresamos al login de Facebook');
        this.fb.login(['public_profile', 'email'])
            .then((res: FacebookLoginResponse) => this.getUserInfo(res.authResponse.userID))
            .catch(e => this.loginFacebookError(e));
    }

    getUserInfo(userId: string) {
        this.fb.api('me?fields=' + ['name', 'email', 'first_name', 'last_name', 'picture.type(large)'].join(), null)
            .then((res: any) => this.setFacebookUserInfo(res))
            .catch(e => this.loginFacebookError(e));
    }

    setFacebookUserInfo(user: any) {
        this.user = user;
        this.router.navigateByUrl('dashboard');
    }

    loginFacebookError(error: any) {      
        console.info('Error logging into Facebook', error);
    }
}
