export class User {
    id: number;
    name: string;    
    email: string;
    birthday: string;
    gender: string;
    mobile: string;
    password: string;    
}