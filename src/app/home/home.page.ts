import { MenuController, NavController, Platform, ToastController, LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Component } from '@angular/core';
import { Router } from  "@angular/router";
import { Storage } from '@ionic/storage';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public unregisterBackButtonAction: any;

  constructor(public navCtrl: NavController,  public platform: Platform, public loadingCtrl: LoadingController, public menu: MenuController, private storage: Storage,
  	 		  private  router:  Router, public translate: TranslateService, private authenticationService: AuthenticationService) {

  }
   
  ngOnInit() {
   
  }
   
  ionViewWillEnter() {	
	this.menu.enable(false);
	//this.initializeBackButtonCustomHandler();
    this.storage.get('tutorial').then((result) => {
      //console.log("El tutorial ya se vio!");
      //console.log(result);
      if(result){
        this.storage.get('loginSession').then((val) => {
          //console.log('El estado de la sesión es: ', val);
          if (val == "true"){
            //console.log("true");
            // En caso de que ya se vio el estado de la sesion de manera local, se rectifica mediante el estado del Guard
            this.authenticationService.authenticationState.subscribe(state => {
              //console.log(state);
              if (state) {
                //console.log(state);
                this.router.navigateByUrl('dashboard');
                               
              } else {
              
              }
            });            
          }
         
        });
      } else {
        this.router.navigateByUrl('tutorial');        
      }
      
    });
    return;
  }

  /*initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.backButton.subscribe(() => {
	  // Interceptamos el presionar el botón atras
	  })
  }*/

   ionViewWillLeave() {   
    // Estas ´líneas comentadas hacen que el usuario al presionar el botón fisico atras impida cerrar la app
    //this.unregisterBackButtonAction && this.unregisterBackButtonAction();   
  }

}
