import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },  
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },  
  { path: 'inicio', loadChildren: './pages/inicio/inicio.module#InicioPageModule' },
  { path: 'dashboard', loadChildren: './pages/dashboard/dashboard.module#DashboardPageModule', canActivate: [AuthGuard] },
  { path: 'condiciones', loadChildren: './pages/condiciones/condiciones.module#CondicionesPageModule', canActivate: [AuthGuard] },
  { path: 'tutorial', loadChildren: './pages/tutorial/tutorial.module#TutorialPageModule' },
  { path: 'detail', loadChildren: './pages/detail/detail.module#DetailPageModule', canActivate: [AuthGuard] },
  { path: 'cart', loadChildren: './pages/cart/cart.module#CartPageModule', canActivate: [AuthGuard] },
  { path: 'user', loadChildren: './pages/user/user.module#UserPageModule', canActivate: [AuthGuard] },
  { path: 'terms', loadChildren: './pages/terms/terms.module#TermsPageModule', canActivate: [AuthGuard] },
  { path: 'about', loadChildren: './pages/about/about.module#AboutPageModule', canActivate: [AuthGuard] },
  { path: 'privacidad', loadChildren: './pages/privacidad/privacidad.module#PrivacidadPageModule', canActivate: [AuthGuard] },
  { path: 'card', loadChildren: './modals/card/card.module#CardPageModule', canActivate: [AuthGuard] },
  { path: 'verify', loadChildren: './pages/verify/verify.module#VerifyPageModule', canActivate: [AuthGuard] },
  { path: 'recover', loadChildren: './pages/recover/recover.module#RecoverPageModule' },
  { path: 'ticket', loadChildren: './pages/ticket/ticket.module#TicketPageModule' }


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
