import { Component, OnInit } from '@angular/core';
import { MenuController, NavController, Platform, ToastController, LoadingController, ModalController } from '@ionic/angular';
import { Router } from  "@angular/router";
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { map } from 'rxjs/operators';

//import { Stripe } from '@ionic-native/stripe/ngx'; <---- era el plugin viejo 
declare var Stripe;
import { HttpClient } from "@angular/common/http";
import { NavParams } from '@ionic/angular';

//Services
import { EnvService } from 'src/app/services/env.service';
import { AlertService } from 'src/app/services/alert.service';
import { CartService, Item } from '../../services/cart.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.page.html',
  styleUrls: ['./card.page.scss'],
})
export class CardPage implements OnInit {
  stripe:any;
  test:any;
  
  card: any;  
  currency: string = 'MXN';
  currencyIcon: string = '$';
  loaderToShow: any;
  clientSecret: any;
  private myToast: any;
  public payAmount: string;
  form: { firstName: string, lastName: string, email:string, address: string} = {
      firstName: '',
      lastName: '',
      email: '',
      address: ''
  };
  orderData: {
    items: [{ id: 0, descripcion: string, precio: 0 }],
    currency: string,
    email : string,
    idcliente: 0,
    idpedido: 0
  } = { items: [{ id: 0, descripcion: '', precio: 0 }], 
    currency: '',
    email : '',
    idcliente: 0,
    idpedido: 0
  };
  account: { estado: 0, id: 0, name: string, email:string, birthday: string, gender: string, phone: string, password: string, terms: 0} = {
      estado: 0,
      id: 0,
      name: '',
      email: '',
      birthday: '',
      gender: '',
      phone: '',
      password: '*****',
      terms: 0
  };
  pedido: any;
  sucursal : any;
  sucursales : any;
  token: any;
  arreglo: any = [];
  loader: any = null;
  items: any;
  confirmacion: any;
  email: any;
  constructor(public navCtrl: NavController,  public platform: Platform, public loadingController: LoadingController, public menu: MenuController,
              private  router: Router, public alert: AlertService, private service:EnvService, private storage: Storage, private cartService: CartService,
              private navParams: NavParams, public toastController: ToastController, public modalController: ModalController) { 
   
    this.loadItems(); 
   
    
  }
  
 
  
  ngOnInit() {
    this.showLoader('Cargando articulos del carrito...');
  	Promise.all([this.storage.get('sucursal_seleccionada'), this.storage.get('name'), this.storage.get('email'), this.storage.get('date')
      , this.storage.get('genero'), this.storage.get('phone'), this.storage.get('sucursales'), this.storage.get('sucursal'), 
        this.storage.get('token'), this.storage.get('id'), this.storage.get('fecha_min_entrega'), this.storage.get('fecha_max_entrega'), this.storage.get('pedido'), 
        this.storage.get('my-items')]).then(values => {
          this.account.name = values[1];
          this.account.email = values[2];

          //console.log(this.account.email);
          this.account.birthday = values[3];
          this.account.gender = values[4];
          this.account.phone = values[5];
          this.sucursales = values[6]
          this.sucursal = values[7];
          this.token = values[8];
          this.account.id = values[9];       
          this.pedido = values[12];
         
          //console.log(this.pedido['pedido'].id); 

              
          this.orderData.email = this.account.email;
          this.orderData.idcliente = this.account.id;
          this.orderData.idpedido = this.pedido['pedido'].id;

          this.orderData.currency = "MXN";
         // id: "1" ,descripcion : "pastelx" , precio : "100"
          this.pedido['productos'].forEach( (item, index) => {  
              //let fecha = moment(this.fecha).format('YYYY-MM-DD');
               this.arreglo.push({id:item.pedido_id,descripcion:item.chispas,precio:item.precio}); 
              //console.log(item);
              //console.log(fecha);
              
              
          });
         this.orderData.items = this.arreglo;    

         //console.log(this.orderData);
         this.payment();
         var form = document.getElementById('payment-form');
            form.addEventListener('submit', event => {
                event.preventDefault();
                //console.log(event)
                //console.log(clientSecret);
                this.pay(this.stripe, this.card, this.clientSecret);
                //this.order();
                
                //this.showLoader('Procesando pago...');
            });     
        });
   
 
  }

  closeModal() {
   
    this.modalController.dismiss({
      'dismissed': true
    });


  }


  setupStripe(key, secret) {
    //console.log('entro al intento');

    let elements = this.stripe.elements();
    var style = {
      base: {
        color: '#32325d',
        lineHeight: '24px',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }     
    };

    this.card = elements.create('card', { style: style });

    //console.log(this.card);
    this.card.mount('#card-element');
    
    //this.card.amount = this.payAmount;
    //console.log(this.card);
    this.hideLoader();
    this.card.addEventListener('change', event => {
      var displayError = document.getElementById('card-errors');
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
    });

   
  }

  
  showLoader(message) {
    this.loaderToShow = this.loadingController.create({
      message: message
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        //console.log('Loading dismissed!');
      });
    });
   
  }

  hideLoader() {
      this.loadingController.dismiss();
  }

  showToast(message) {
    this.myToast = this.toastController.create({
      message: message,
      duration: 2000
    }).then((toastData) => {
      //console.log(toastData);
      toastData.present();
    });
  }

  payment(){  
    
     this.storage.get('token').then((token) => {
     this.service.post_token( 'pagos', this.orderData, token )
      .pipe(map(data => {     

         let publishableKey = data['publishableKey'];
         let clientSecret=data['clientSecret'];
         this.clientSecret = data['clientSecret'];
         //console.log('**************************************************************************************************************');
         //console.log('**************************************************************************************************************');
         //console.log(publishableKey);
         this.stripe = Stripe(publishableKey);
         //console.log(clientSecret);         
         this.setupStripe(publishableKey, clientSecret);  
         
         
      }))
      .subscribe(
        (data) => {
          
         this.hideLoader();

        },
        (err) => {
          let error = err.error;
          //console.log(err['msg']);
          this.hideLoader();
          if(error.message == 'Validation Error.'){
             this.hideLoader();
            this.alert.show('Alerta','Mensaje','Validation Error');
          } else {
             this.hideLoader();
            //CACHAR ERROR DE REGISTRO DUPLICADO
            this.alert.show('Alerta','Mensaje',error['msg']);
          }
      });
    });  
  }
 

  pay(stripe, card, clientSecret){
    this.showLoader('Procesando pago...');
    this.stripe.confirmCardPayment(clientSecret,{
      payment_method: {
        card: this.card,
        billing_details: {
              address: {
                line1: this.form.address                
              },
              name: this.form.firstName + " " + this.form.lastName,
              phone: this.account.phone,
              email: this.account.email
            },
      },
      receipt_email: this.account.email,
      shipping: {
          name: this.form.firstName + " " + this.form.lastName,
          phone: this.account.phone,   
          address: {
            line1: this.form.address,
            state: null,
            country: null,
          },
      },
    })
    .then((result) => {
      //console.log(result);
       if (result.error) {
          var errorElement = document.getElementById('card-errors');
          errorElement.textContent = result.error.message;  
          this.hideLoader();        
        } else {
         this.newOrder();
       }
    });

  }

  newOrder(){
    //console.log();
    //this.order(clientSecret);      /// AL LLEGAR A ESTE PUNTO YA NO ME DEJA EJECTUTAR MAS FUNCIONES       
    this.stripe.retrievePaymentIntent(this.clientSecret).then((res:any) => {
      var paymentIntent = res.paymentIntent;   
      var paymentIntentJson = JSON.stringify(paymentIntent, null, 2); /// NECESITO GUARDAR ESTO PARA VOLVERLO A OCUPAR     
      
      //console.log(paymentIntent);
      this.storage.set('payment', paymentIntentJson);  
      this.hideLoader(); 
      this.showToast('¡Gracias por su compra, pago exitoso!');
      if (paymentIntent.status == "succeeded"){        
        this.closeModal();
        this.router.navigateByUrl('ticket');
      } else {
        //console.log('VALIO VERGA SU PEDIDO, LA CULPA ES DEL PROGRAMADOR'); // <---------
      }
    });
   

  }

  async loadItems() {
    await this.cartService.getItems().then(items => {
      this.items = items;
      //console.log('Items a comprar');     
      //console.log(this.items);      
    });   
  }

}
