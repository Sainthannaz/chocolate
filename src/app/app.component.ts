import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Platform, Config, AlertController, LoadingController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Storage } from '@ionic/storage';
import { Router } from  '@angular/router';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { EnvService } from 'src/app/services/env.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { CartService, Item } from './services/cart.service';
import { Cakes } from "./mocks/providers/cakes";
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  alertText: any = {};
  loader: any;
  items: Item[] = [];
  harcodeData: any;
  allData:any;
  soloPastelesEnLinea: any;
  public appPages = [
    {
      title: 'MENU_DASHBOARD',
      url: '/dashboard',
      icon: 'home'
    }, 
     
    {
      title: 'MENU_USER',
      url: '/user',
      icon: 'cog'
    }
  ];
  
  userData: { uid: string, email: string } = {
    uid: '',
    email: ''
  };

  hardcodeData: any = [
      {"id":1, "description":"Pastel de chocolate", "name": "Chocolate", "price":"", "peso":"", "pan":"1", "relleno":"4", "cubierta": "2", "image": "../assets/imgs/1.png"},
      {"id":2, "description":"Pastel de queso con chocolate", "name": "Queso con Chocolate", "price":"", "peso":"", "pan":"3", "relleno":"1", "cubierta": "2", "image": "../assets/imgs/2.png"},
      {"id":3, "description":"Pastel de chocoqueso", "name": "Chocoqueso", "price":"", "peso":"", "pan":"1", "relleno":"1", "cubierta": "2", "image": "../assets/imgs/3.png"},      
      {"id":4, "description":"Pastel de tres leches con chocolate", "name": "3L con Chocolate", "price":"", "peso":"", "pan":"4", "relleno":"4", "cubierta": "2","image": "../assets/imgs/7.png"},     
      {"id":5, "description":"Pastel Marmoleado", "name": "Marmoleado", "price":"", "peso":"", "pan":"2", "relleno":"4", "cubierta": "2","image": "../assets/imgs/6.png"}

     

  ];

  
  constructor(
    private platform: Platform, public toastCtrl: ToastController, private screenOrientation: ScreenOrientation,
    private splashScreen: SplashScreen, private config: Config, public loadingCtrl: LoadingController, public cakes: Cakes,
    private statusBar: StatusBar, private translate: TranslateService, private storage: Storage, private router: Router, 
    public alertCtrl: AlertController, private service: EnvService,  private authService: AuthenticationService, private cartService: CartService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);
      this.statusBar.backgroundColorByHexString('#D25D12');  
      this.loadProducts();
      //console.log("Hardcode data");
      
      
      console.log(this.hardcodeData);   
      console.log(this.cakes);   
      //this.storage.set('pasteles_locales', this.hardcodeData); 
      this.storage.set('pasteles_locales', this.cakes);       
      //this.storage.set('sucursal_seleccionada', 'false'); 
      // Request a catalogos de inicio de productos
     
      this.splashScreen.hide();
      
    });



    this.initTranslate();
    // orientacion actual
    //console.log(this.screenOrientation.type); 

    // fijamos a tipo retrato
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);

    this.translate.get('ALERT_TITLE_EXIT').subscribe(title => {
      this.alertText.title = title;
    });

    this.translate.get('ALERT_MESSAGE_EXIT').subscribe(message => {
      this.alertText.message = message;
    });

    this.translate.get('ALERT_OK').subscribe(ok => {
      this.alertText.ok = ok;
    });

    this.translate.get('ALERT_CANCEL').subscribe(cancel => {
      this.alertText.cancel = cancel;
    });
  }

  initTranslate() {    
    this.translate.setDefaultLang('es');
    const browserLang = this.translate.getBrowserLang();    
    this.translate.use('es'); // Set your language here
    
    // Muestra el texto Atras en la toolbar
    /*this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('backButtonText', values.BACK_BUTTON_TEXT);
    });*/     
  }
  
  closeSession(){
     this.presentAlert();       
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      message: 'Auth...',
      spinner: 'crescent',
      duration: 2000
    });
    this.loader.present();
  }

  async showLongToast() {
    const toast = await this.toastCtrl.create({
      message: '...',
      duration: 4000,
      position: 'bottom'
    });
    toast.present();
  }

  async presentAlert() {
    //console.log('Alert Event');
    const alert = await this.alertCtrl.create({
      header: this.alertText.title,    
      message: this.alertText.message,
      buttons: [ {
          text: this.alertText.cancel,
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            //console.log('Confirm Cancel');
          }
        }, {
          text:  this.alertText.ok,
          handler: () => {
            //console.log('Confirm Ok');
             this.storage.set('loginSession', 'false');
             this.storage.set('sucursal_seleccionada', 'false');
             this.storage.set('verify', 'false');
             this.deleteCart();
             this.authService.logout();
             this.router.navigateByUrl('home');    
          }
        }]
    });

    await alert.present();
  }

  verify(){
     this.router.navigateByUrl('verify');    
    
  }

  async deleteCart() {
    await this.cartService.deleteAll().then(items => {
     
    },  (err) => { console.log(err);} ); 
  }

  loadProducts(){
   let params = {       
    
    }
   this.storage.get('token').then((token) => {
    this.service.get('productos/sg', params, token)
     .pipe(map(data => {    
         //console.log(data['msg']); 
         console.log(data);
         this.cakes.deleteAll();     
         this.allData = data;
          this.allData.forEach( (item, index) => {  
            //console.log(item.ventaenlinea);
            if(item.ventaenlinea == 1){
              console.log(item);            
              this.soloPastelesEnLinea = ({"id":item.id, "IdProducto":item.IdProducto, "Tipo":item.Tipo, "Unidadsat":item.Unidadsat, "Codigosat":item.Codigosat, "activo":item.activo, "created_at":item.created_at, "updated_at":item.updated_at, "grupo":item.grupo, "idsucursal":item.idsucursal, "tasa":item.tasa , "description":item.description,"name":item.description,"price":item.precio,"peso":item.Peso,"pan":item.Pan,"relleno":item.Relle,"cubierta":item.Cubi,"image":item.foto });
              this.cakes.add(this.soloPastelesEnLinea);
              
            }

          })    
         //this.someTestCakes = data;
     }))     
     .subscribe(
      (data) => {       
           
      },
      (err) => {
         console.log(err);
         let error = err['error'].msg;
         console.error(error);
         
      });
   });

 }
}
