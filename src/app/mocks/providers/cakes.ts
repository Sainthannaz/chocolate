import { Injectable } from '@angular/core';

import { Cake } from '../../models/cake';

//@Injectable()
export class Cakes {
  cakes: Cake[] = [];

  defaultCake: any = {

  };


  constructor() {
    let cakes = [

    ];

    for (let cake of cakes) {
      this.cakes.push(new Cake(cake));
    }
  }

  query(params?: any) {
    if (!params) {
      return this.cakes;
    }

    return this.cakes.filter((cake) => {
      for (let key in params) {
        let field = cake[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return cake;
        } else if (field == params[key]) {
          return cake;
        }
      }
      return null;
    });
  }

  add(cake: Cake) {
    this.cakes.push(cake);
  }

  delete(cake: Cake) {
    this.cakes.splice(this.cakes.indexOf(cake), 1);
  }

  deleteAll() {
    this.cakes.splice(0);
  }
}
