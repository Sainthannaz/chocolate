import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Storage } from '@ionic/storage';


import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EnvService {
  
    //PRODUCCIÓN    
    //private url: string = 'https://apichocolate.net/api/v1';
    
    private url: string = 'http://servidorapitestbeta-env.eba-qynim3u2.us-east-2.elasticbeanstalk.com/api/v1';
    // LOCAL
    //private url: string = 'http://127.0.0.1:8000/api/v1';
    constructor( private http: HttpClient, private storage: Storage ) {
    }

    post(action, params) {
      let options = {
        headers: {
          'Access-Control-Allow-Origin' : '*',
          'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
          'Accept':'application/json',
          'content-type':'application/json'
        },
        withCredentials: true
      };
      return this.http.post(this.url + '/' + action, JSON.stringify(params), options);
    }

    post_token(action, params, token) {      

      let options = {
        headers: {
          'Access-Control-Allow-Origin' : '*',
          'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
          'Accept':'application/json',
          'content-type':'application/json',
          'Authorization': 'Bearer ' + token
        },
        withCredentials: true
      };
      return this.http.post(this.url + '/' + action, JSON.stringify(params), options);
    }
    
    post_pay_token(action, paymentIntentJson, token) {      
      let options = {
        headers: {
          'Access-Control-Allow-Origin' : '*',
          'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
          'Accept':'application/json',
          'content-type':'application/json',
          'Authorization': 'Bearer ' + token
        },
        body:  paymentIntentJson,             
        withCredentials: true,
      };
      
      return this.http.post(this.url + '/' + action, options);
    }

    get(action, params, token) {
      let options = {
        headers: {
          'Access-Control-Allow-Origin' : '*',
          'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
          'Accept':'application/json',
          'content-type':'application/json',
          'Authorization': 'Bearer ' + token
        },
        withCredentials: true
      };
      return this.http.get(this.url + '/' + action, options);
    }

    simple_get(action, params) {
      let options = {
        headers: {
          
          'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
          'Accept':'application/json, text/plain, */*',
          'content-type':'application/json',
          'Authorization': 'Bearer ' + null
        },
        withCredentials: true
      };
      console.log(params);
      return this.http.get(this.url + '/' + action + '/' + params, options);
    }

    simple_get_token(action, params, token) {
      let options = {
        headers: {
          
          'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
          'Accept':'application/json, text/plain, */*',
          'content-type':'application/json',
          'Authorization': 'Bearer ' + token          
        },
        withCredentials: true
      };
      console.log(params);
      return this.http.get(this.url + '/' + action + '/' + params, options);
    }

    get_token(action, params, token) {      
      let options = {
        headers: {
          'Access-Control-Allow-Origin' : '*',
          'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
          'Accept':'application/json',
          'content-type':'application/json',
          'Authorization': 'Bearer ' + token
        },
        withCredentials: true
      };
      return this.http.get(this.url + '/' + action, options);
    }

    put_token(action, params, token) {      
      let options = {
        headers: {
          'Access-Control-Allow-Origin' : '*',
          'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
          'Accept':'application/json',
          'content-type':'application/json',
          'Authorization': 'Bearer ' + token
        },
        withCredentials: true
      };
      return this.http.put(this.url + '/' + action,  JSON.stringify(params), options);
    }
}
