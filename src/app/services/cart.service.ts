import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ToastController  } from '@ionic/angular';

export interface Item {
  id: number,
  producto_id:number,
  descripcion: string,
  leyenda: string,
  chispas: string,
  cantidad: string,
  pan: string,
  relle: string,
  cubi: string,
  peso: string,
  precio: number,
  fecha_entrega: string
}

const ITEMS_KEY = 'my-items';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private storage: Storage, public toastCtrl:ToastController ) { 
    
  }
  
  // CREATE
  addItem(item: Item): Promise<any> {
    return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
      if (items) {
          console.log(items.length); 
        if (items.length <= 2){            
          items.push(item);
        } else { 
          this.showToast('Lo sentimos, no se pueden agregar mas de tres articulos');
        }
        return this.storage.set(ITEMS_KEY, items);
      } else {
        return this.storage.set(ITEMS_KEY, [item]);
      }
    });
  }
 
  // READ
  getItems(): Promise<Item[]> {
    return this.storage.get(ITEMS_KEY);
  }
 
  // UPDATE
  updateItem(item: Item): Promise<any> {
    return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
      if (!items || items.length === 0) {
        return null;
      }
 
      let newItems: Item[] = [];
 
      for (let i of items) {
        if (i.id === item.id) {
          newItems.push(item);
        } else {
          newItems.push(i);
        }
      }
 
      return this.storage.set(ITEMS_KEY, newItems);
    });
  }
 
  // DELETE
  deleteItem(id: number): Promise<Item> {
    return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
      if (!items || items.length === 0) {
        return null;
      }
 
      let toKeep: Item[] = [];
 
      for (let i of items) {
        if (i.id !== id) {
          toKeep.push(i);
        }
      }
      return this.storage.set(ITEMS_KEY, toKeep);
    });
  }
  
  deletePos(pos: number): Promise<Item> {
    return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
       if (!items || items.length === 0) {
        return null;
      }
      console.log(items);   
      for (let i of items) {
        console.log(i);        
      }
      return this.storage.get(ITEMS_KEY);
    });
  }

  //DELETE ALL 
  deleteAll( ): Promise<Item> {
    return this.storage.remove(ITEMS_KEY).then((items: Item[]) => {
       if (!items || items.length === 0) {
        items.splice(0); 
        return null;       
      }
     
      return this.storage.get(ITEMS_KEY);
    });
  }

  async showToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }  
}
