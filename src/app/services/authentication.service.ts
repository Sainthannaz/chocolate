import { Platform, NavController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';

 
const TOKEN_KEY = 'auth-token';
 
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
 
  authenticationState = new BehaviorSubject(false);
 
  constructor(private storage: Storage, private plt: Platform, public navCtrl: NavController) { 
    this.plt.ready().then(() => {
      this.checkToken();
    });
  }
 
  checkToken() {
    this.storage.get(TOKEN_KEY).then(res => {
      if (res) {
        this.authenticationState.next(true);
      }
    })
  }
  

  login(login, token, name, email, fechanacimiento, celular, genero, foto, id, email_verified_at) {
    console.log();
    let promises = [
      this.storage.set(TOKEN_KEY, 'Bearer 1234567'),
      this.storage.set('loginSession', login),
      this.storage.set('token', token),
      this.storage.set('name', name),
      this.storage.set('email', email),
      this.storage.set('fechanacimiento', fechanacimiento),
      this.storage.set('celular', celular),
      this.storage.set('genero', genero),
      this.storage.set('foto', foto),
      this.storage.set('id', id),
      this.storage.set('email_verified_at', email_verified_at),
    ];

    Promise.all(promises)
      .then(data => {
          this.authenticationState.next(true);
          this.navCtrl.navigateRoot('dashboard');
      })
      .catch((err) => {
          
      });  
  }
  
  register(login, token, name, email, fechanacimiento, celular, genero, foto, id, email_verified_at) {
    console.log();
    let promises = [
      this.storage.set(TOKEN_KEY, 'Bearer 1234567'),
      this.storage.set('loginSession', login),
      this.storage.set('token', token),
      this.storage.set('name', name),
      this.storage.set('email', email),
      this.storage.set('fechanacimiento', fechanacimiento),
      this.storage.set('celular', celular),
      this.storage.set('genero', genero),
      this.storage.set('foto', foto),
      this.storage.set('id', id),
      this.storage.set('email_verified_at', email_verified_at),
    ];

    Promise.all(promises)
      .then(data => {
          this.authenticationState.next(true);
          this.navCtrl.navigateRoot('verify');
      })
      .catch((err) => {
          
      });
   
  }
  
  logout() {
    return this.storage.remove(TOKEN_KEY).then(() => {
      this.authenticationState.next(false);
    });
  }
 
  isAuthenticated() {
    return this.authenticationState.value;
  }
 
}