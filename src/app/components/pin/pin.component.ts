import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AlertController, LoadingController, ToastController, ModalController } from '@ionic/angular';

@Component({
  selector: 'custom-pin',
  templateUrl: './pin.component.html',
  styleUrls: ['./pin.component.scss'],
})

export class PinComponent {
  @Input() pagetitle: String = "Enter Pin";

  pin:string= "";

  @Output() change: EventEmitter<string> = new EventEmitter<string>();
  constructor( public toastCtrl: ToastController, public loadingCtrl: LoadingController) { 

  }

  emitEvent() {
    this.change.emit(this.pin);
    console.log(this.pin);
  }

  handleInput(pin: string) {
    if (pin === "clear") {
      this.pin = "";
      return;
    }
    
    if (this.pin.length === 6) {
      return;
    } 
    this.pin += pin;
    
  }

  async showToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}
