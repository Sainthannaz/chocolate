import { NgModule } from '@angular/core';
import { PinComponent } from './pin/pin.component'
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

@NgModule({
	imports: [CommonModule, IonicModule],
	declarations: [ PinComponent ],
	exports: [ PinComponent ],
	
})

export class ComponentsModule{}